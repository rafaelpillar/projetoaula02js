function ordenar(array) {
	for (let i = 0; i < array.length; i++) {
		for (let j = 0; j < array.length; j++) {
			if (array[j] > array[j + 1]) {
				let temp = array[j];
				array[j] = array[j + 1];
				array[j + 1] = temp;
			}
		}
	}
	console.log(array)
}

function createArray(tamanho,callback){
    const numeros = []; 
    for(let i = 0;i<tamanho;i++){
        numeros.push(Math.round(Math.random()*100))
    }
    callback(numeros)
   }

 createArray(4,ordenar)

